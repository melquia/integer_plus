#include <ruby.h> // include the ruby.h header files

// 
// the VALUE is built in type of the Ruby
// 
// 
VALUE plus(VALUE from, VALUE val) {
  // 
  // convert the VALUE to long
  // 
  long num = FIX2LONG(from) + FIX2LONG(val);

  // 
  // format the string
  // 
  VALUE str = rb_sprintf("num %ld", num);
  // 
  // print the string
  // 
  rb_p(str);

  // 
  // convert the long to VALUE and return
  // 
  return INT2NUM(num);
}

// 
// the init method mush start with Init and end with underline and source file name
// 
void Init_plus() {
  // 
  // rb_cInteger is the Intger class of the Ruby
  // "plus" is the binding name for ruby and c
  // plus is the function
  // 1 means how much args that needed by the method
  rb_define_method(rb_cInteger, "plus", plus, 1);

  // define a global const
  rb_define_global_const("MEL", INT2NUM(0));

  // rb_define_const
  rb_define_const(rb_cInteger, "NORMAL_TEMP", INT2NUM(100));
}


// void rb_define_method(VALUE klass, const char *name,
//                       VALUE (*func)(ANYARGS), int argc)

// void rb_define_singleton_method(VALUE object, const char *name,
//                                 VALUE (*func)(ANYARGS), int argc)
